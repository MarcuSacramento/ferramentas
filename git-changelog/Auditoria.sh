#!/bin/bash

        data_atual=$(date +"%Y-%m-%d")
                nome_grupo=$1
                nome_projeto=$2
                branch=$3
                echo '###################  Removendo Estrutura antiga ###################'
                rm -rf 'C:\Projetos\Arquivos\Auditoria\'$nome_grupo'\repo_'$nome_projeto
                echo '###################  Criando Estrutura  ###################'
                mkdir 'C:\Projetos\Arquivos\Auditoria\'$nome_grupo
                mkdir 'C:\Projetos\Arquivos\Auditoria\'$nome_grupo'\'$nome_projeto
                caminho_log='C:\Projetos\Arquivos\Auditoria\'$nome_grupo'\'$nome_projeto'\'$data_atual
                rm -Rf $caminho_log

                mkdir $caminho_log

		#export PATH=$PATH:/home/git/repo_auditoria/git-changelog
                cd 'C:\Projetos\Arquivos\Auditoria\'$nome_grupo


                echo '###################  Realizando Clone do Projeto '$nome_projeto' ###################'
                #git clone http://git.mj.gov.br/$nome_grupo/$nome_projeto.git
                git clone git@git.mj.gov.br:$nome_grupo/$nome_projeto.git 'repo_'$nome_projeto
                cd 'repo_'$nome_projeto
                pwd
                echo '###################  Realizando Checkout do Branch '$branch' ###################'
                git checkout $branch

                echo '###################  Realizando Atualização do Branch '$branch' ###################'
                git pull origin $branch

                log_arquivos=$caminho_log'\'$data_atual'-'$nome_grupo'_'$nome_projeto'_arquivos_'$branch'.log'
                log_commit=$caminho_log'\'$data_atual'-'$nome_grupo'_'$nome_projeto'_commit_'$branch'.log'


                echo '###################  Gerando Log dos arquivos no '$branch' ###################'
                find  \( ! -regex '.*/\..*' \) -type f -printf '%TY%Tm%Td-%TH%TM\t%AY%Am%Ad-%AH%AM\t%s\t%h/%f\n'  > $log_arquivos

                echo '###################  Gerando Log dos commits '$branch' ###################'
                git log --oneline --date-order --decorate --color --all --pretty=format:"\"%ai\",\"%s\",\"%an-%ae\",\"%d\",\"%h\"" $branch >  $log_commit
				
		echo '###################  Para Gerar o Changelog ###################'
                echo 'Execute o comando: cd C:\Projetos\Arquivos\Auditoria\'$nome_grupo'\repo_'$nome_projeto' && git changelog > C:\Projetos\Arquivos\Auditoria\'$nome_grupo'\'$nome_projeto'\'$data_atual'\CHANGELOG.md'

		#git changelog > 'C:\Projetos\Arquivos\Auditoria\'$nome_grupo'\'$nome_projeto'\'$data_atual'\CHANGELOG.md'
				
                
                #echo '###################  Removendo Diretório do Projeto  '$nome_projeto' ###################'
                #rm -Rf $nome_projeto
                #echo '###################  Geração de Arquivos Concluída ###################'
                read

