require 'date'

def print_stats(fmt, label, commits)
  a = r = 0
  days = []
  commits.each { |c| a += c[1]; r += c[2]; days << c[0] }
  c = days.uniq.size
  puts(fmt % [label, a, a / c, r, r / c, c, commits.size])
end

def sum_added(arr)
  arr.inject(0) { |s, v| s + v[1] }
end

def sorted_stat_keys(stats)
  stats.keys.sort { |a, b| sum_added(stats[a]) <=> sum_added(stats[b]) }
end

stats = Hash.new { |h, k| h[k] = [] }
cur_author = nil
cur_commit = nil

while !STDIN.eof?
  line = STDIN.readline.strip
  if m = line.match(/^(\d+)\s+(\d+)\s+\S/)
    added, removed = m[1..2].map { |x| x.to_i }
    cur_commit[1] += added
    cur_commit[2] += removed
  elsif m = line.match(/^([^-].+)\|(.+)/)
    cur_author = m[1]
    cur_commit = [DateTime.parse(m[2]).strftime('%F'), 0, 0]
    stats[cur_author] << cur_commit
  end
end

maxlen = (stats.keys.map { |x| x.size }.max) +8
#fmt = "%-#{maxlen}s %10d %10d %10d %10d %10d %10d"
#puts("%-#{maxlen}s      added    add/day    removed    rem/day  uniq days    commits" % "Autor")

fmt = "| %-#{maxlen}s| %12d| %12d| %12d| %12d| %12d| %12d|"
puts "Esta Estatística foi gerada automaticamente."
puts ""
puts "Veja https://gist.github.com/chrismdp para mais informações."
puts "###### *(MP) = Most most-prolific, ou Mais Produtivo. Considera apenas o centro de 90% dos Commits*"
puts ""
puts("| %-#{maxlen}s|"% "Autor"+" %-#{12}s|" %"Adições"+" %-#{12}s|" % "Adição/Dia"+" %-#{12}s|" % "Remoções"+" %-#{12}s|" % "Remoção/Dia"+" %-#{12}s|" % "Dias"+" %-#{12}s|" % "Commits")
puts("|-%-#{maxlen}s|"% ("-" * maxlen) +"-------------|-------------|-------------|-------------|-------------|-------------|"  )

sorted_stat_keys(stats).each do |committer|
  commits = stats[committer].sort { |a, b| a[1] <=> b[1] }
  print_stats(fmt, committer, commits)

  s = commits.size
  #reject 10% less- and 10% most-prolific commits
  lo, hi = (s * 0.05).round, (s * 0.95).round
  commits = commits[lo..hi]
  print_stats(fmt, "**"+committer+"(MP)**", commits)

end

#git log --numstat --pretty=tformat:"%an|%ad" --date=short | ruby C:\Projetos\Ferramentas\git-changelog\stat.rb

git-contributors
git-release-note
git-time-log
git-commit-summary